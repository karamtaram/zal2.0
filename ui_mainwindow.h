/********************************************************************************
** Form generated from reading UI file 'mainwindowXTJ348.ui'
**
** Created by: Qt User Interface Compiler version 4.8.7
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef MAINWINDOWXTJ348_H
#define MAINWINDOWXTJ348_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QListView>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QSplitter>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QTreeView>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *action_Open_file;
    QAction *action_Quit;
    QAction *action_Close_file;
    QAction *actionExport_to;
    QAction *actionDeselect_all;
    QWidget *centralWidget;
    QGridLayout *gridLayout_2;
    QSplitter *splitter;
    QTreeView *treeView;
    QListView *imageSelector;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(800, 600);
        MainWindow->setContextMenuPolicy(Qt::DefaultContextMenu);
        MainWindow->setToolButtonStyle(Qt::ToolButtonTextOnly);
        action_Open_file = new QAction(MainWindow);
        action_Open_file->setObjectName(QString::fromUtf8("action_Open_file"));
        action_Quit = new QAction(MainWindow);
        action_Quit->setObjectName(QString::fromUtf8("action_Quit"));
        action_Close_file = new QAction(MainWindow);
        action_Close_file->setObjectName(QString::fromUtf8("action_Close_file"));
        action_Close_file->setEnabled(false);
        actionExport_to = new QAction(MainWindow);
        actionExport_to->setObjectName(QString::fromUtf8("actionExport_to"));
        actionExport_to->setEnabled(false);
        actionDeselect_all = new QAction(MainWindow);
        actionDeselect_all->setObjectName(QString::fromUtf8("actionDeselect_all"));
        actionDeselect_all->setEnabled(false);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        gridLayout_2 = new QGridLayout(centralWidget);
        gridLayout_2->setSpacing(6);
        gridLayout_2->setContentsMargins(11, 11, 11, 11);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        splitter = new QSplitter(centralWidget);
        splitter->setObjectName(QString::fromUtf8("splitter"));
        splitter->setEnabled(true);
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(splitter->sizePolicy().hasHeightForWidth());
        splitter->setSizePolicy(sizePolicy);
        splitter->setFrameShape(QFrame::NoFrame);
        splitter->setOrientation(Qt::Horizontal);
        splitter->setOpaqueResize(true);
        treeView = new QTreeView(splitter);
        treeView->setObjectName(QString::fromUtf8("treeView"));
        treeView->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        treeView->setEditTriggers(QAbstractItemView::NoEditTriggers);
        treeView->setProperty("showDropIndicator", QVariant(false));
        treeView->setDragDropOverwriteMode(false);
        treeView->setSelectionMode(QAbstractItemView::SingleSelection);
        treeView->setSelectionBehavior(QAbstractItemView::SelectItems);
        splitter->addWidget(treeView);
        treeView->header()->setVisible(false);
        treeView->header()->setDefaultSectionSize(0);
        imageSelector = new QListView(splitter);
        imageSelector->setObjectName(QString::fromUtf8("imageSelector"));
        imageSelector->setFocusPolicy(Qt::StrongFocus);
        imageSelector->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        imageSelector->setAutoScroll(true);
        imageSelector->setEditTriggers(QAbstractItemView::NoEditTriggers);
        imageSelector->setTabKeyNavigation(false);
        imageSelector->setProperty("showDropIndicator", QVariant(false));
        imageSelector->setDragDropOverwriteMode(false);
        imageSelector->setSelectionMode(QAbstractItemView::MultiSelection);
        imageSelector->setVerticalScrollMode(QAbstractItemView::ScrollPerPixel);
        imageSelector->setHorizontalScrollMode(QAbstractItemView::ScrollPerPixel);
        imageSelector->setViewMode(QListView::IconMode);
        imageSelector->setSelectionMode(QAbstractItemView::SingleSelection);
        imageSelector->setSelectionBehavior(QAbstractItemView::SelectItems);
        imageSelector->setSelectionRectVisible(true);
        splitter->addWidget(imageSelector);

        gridLayout_2->addWidget(splitter, 0, 0, 1, 1);

        MainWindow->setCentralWidget(centralWidget);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QString::fromUtf8("mainToolBar"));
        mainToolBar->setEnabled(true);
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        statusBar->setSizeGripEnabled(true);
        MainWindow->setStatusBar(statusBar);

        mainToolBar->addAction(action_Open_file);
        mainToolBar->addSeparator();
        mainToolBar->addAction(action_Close_file);
        mainToolBar->addAction(action_Quit);
        mainToolBar->addSeparator();
        mainToolBar->addAction(actionDeselect_all);
        mainToolBar->addAction(actionExport_to);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "Zal, a HDF nanoBrowser", 0));
        action_Open_file->setText(QApplication::translate("MainWindow", "&Open file", 0));
        action_Open_file->setShortcut(QApplication::translate("MainWindow", "Ctrl+O", 0));
        action_Quit->setText(QApplication::translate("MainWindow", "&Quit", 0));
        action_Quit->setShortcut(QApplication::translate("MainWindow", "Ctrl+Shift+W, Alt+F4, Ctrl+Q", 0));
        action_Close_file->setText(QApplication::translate("MainWindow", "&Close file", 0));
        action_Close_file->setShortcut(QApplication::translate("MainWindow", "Ctrl+W", 0));
        actionExport_to->setText(QApplication::translate("MainWindow", "Export to...", 0));
        actionExport_to->setShortcut(QApplication::translate("MainWindow", "Ctrl+E", 0));
        actionDeselect_all->setText(QApplication::translate("MainWindow", "Deselect all", 0));
        actionDeselect_all->setShortcut(QApplication::translate("MainWindow", "Esc", 0));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // MAINWINDOWXLW348_H
