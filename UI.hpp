#ifndef UI_ZAL_INCLUDED
#define UI_ZAL_INCLUDED

#include "IterableWx.hpp"
#include "ui_mainwindow.h"

#include <QMainWindow>
#include <QFileDialog>

namespace Ui {
class MainWindow;
}


class zalMain: public QMainWindow
{
    Q_OBJECT
public:
    zalMain();
    virtual ~zalMain()
    {
        delete data;
    }
protected:
    void setupActions();
protected slots:
    void openFile();
    void closeFile();
    void loadImageset(const QModelIndex &);
private:
    DataTree *data;
    ModelExIterable *currentImg;
    Ui::MainWindow *ui;
};

#endif /* end of include guard: UI_ZAL_INCLUDED */
