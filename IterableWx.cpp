#include "IterableWx.hpp"
#include "HDFClasses.hpp"

#include <QIcon>
#include <QPixmap>

using namespace HDF;

// Almost default ctor for iterable wrapper
IterableWrapper::IterableWrapper(HDF::Iterable *root, IterableWrapper *parentItem):
    internalPtr{root},
    parentPtr{parentItem}
{
    // empty
}

IterableWrapper::~IterableWrapper()
{
    delete internalPtr;
    internalPtr = nullptr;
}

bool IterableWrapper::hasUnofficialChildren() const
{
    return (internalPtr->getType() == H5I_DATASET);
}

// #500+ xD
// Count the nodes that should not be displayed in the tree
int IterableWrapper::unofficialChildrenCount() const
{
    if (ensureIterated() && hasUnofficialChildren())
        return internalPtr->size();
    else
        return 0;
}

// TODO: generalize this function
std::shared_ptr<Image> IterableWrapper::getUnofficialChild(int row) const
{
    if (ensureIterated() && hasUnofficialChildren()) {
        // Fetch image from container
        Iterable::elementPtr elemPtr = (*internalPtr)[row];

        return std::dynamic_pointer_cast<Image>(elemPtr);
    } else
        // TODO: better exception handling
        throw 1;
}

bool IterableWrapper::ensureIterated() const
{
    if (internalPtr == nullptr)
        return false;

    internalPtr->size();         // forces iteration of vector if it wasn't iterated
    return true;
}

// Returns subelements from iteration
// TODO: try replacing dynamic casts with static cast for performance purposes
IterableWrapper *IterableWrapper::child(int row)
{
    // Fetch element
    std::shared_ptr<HDF::Element> childPtr = (*internalPtr)[row];
    auto childType = childPtr->getType();

    // If it's an iterable element, return iterable child. If it's not, return non-iterable child
    // If it's invalid, return null pointer
    if (childType != H5I_DATASPACE)
        return new IterableWrapper(dynamic_cast<Iterable *>(childPtr.get()), this);
    else
        return nullptr;
}

int IterableWrapper::childCount() const
{
    if (ensureIterated() && !hasUnofficialChildren())
        return internalPtr->size();
    else
        return 0;
}

QVariant IterableWrapper::data(int column) const
{
    if ((column == 0) && ensureIterated())
         return internalPtr->getName().c_str();

    return QVariant{};
}

int IterableWrapper::row() const
{
    // If object is valid
    if ((parentPtr) && (parentPtr->ensureIterated())) {
        // ... find within parent object with same HID. Such object will be in same vector,
        // so we can deduce its location from iterator arithmetics
        auto it = std::find_if(parentPtr->internalPtr->begin(),
                               parentPtr->internalPtr->end(),
                               [this] (const Element &a) -> bool
                               {
                                   return a.isSame(*internalPtr);
                               });
        return it - parentPtr->internalPtr->begin();
    }

    return 0;
}

IterableWrapper *IterableWrapper::parentItem()
{
    return parentPtr;
}

QModelIndex DataTree::index (int row, int column, const QModelIndex &parent) const
{
    if (!hasIndex(row, column, parent))
        return QModelIndex{};

    IterableWrapper *parentPtr;
    if (parent.isValid())
        parentPtr = static_cast<IterableWrapper *>(parent.internalPointer());
    else
        parentPtr = first;

    IterableWrapper *childPtr = parentPtr->child(row);
    if (childPtr)
        return createIndex(row, column, childPtr);
    else
        return QModelIndex{};
}

QModelIndex DataTree::parent(const QModelIndex &index) const
{
    if (!index.isValid())
        return QModelIndex{};

    IterableWrapper *childPtr  = static_cast<IterableWrapper *>(index.internalPointer());
    IterableWrapper *parentPtr = childPtr->parentItem();

    if (parentPtr == first)
        return QModelIndex{};
    else
        return createIndex(parentPtr->row(), 0, parentPtr);
}

int DataTree::rowCount(const QModelIndex &parent) const
{
    if (parent.column() > 0)
        return 0;

    IterableWrapper *parentPtr = nullptr;
    if (parent.isValid())
        parentPtr = static_cast<IterableWrapper *>(parent.internalPointer());
    else
        parentPtr = first;

    return parentPtr->childCount();
}

int DataTree::columnCount(const QModelIndex &) const
{
    return 1;
}

QVariant DataTree::data(const QModelIndex &index, int role) const
{
    if ((index.isValid()) && (role == Qt::DisplayRole)) {
        IterableWrapper *ptr = static_cast<IterableWrapper *>(index.internalPointer());
        return ptr->data(index.column());
    }
    return QVariant{};
}

Qt::ItemFlags DataTree::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return 0;

    // Block object from having children if we'll display them in right pane
    Iterable *ptr = static_cast<IterableWrapper *>(index.internalPointer())->raw();
    if (ptr->getType() == H5I_DATASET)
        return QAbstractItemModel::flags(index) | Qt::ItemNeverHasChildren;
    else
        return QAbstractItemModel::flags(index);
}


ModelExIterable::ModelExIterable(const IterableWrapper *ptr, QObject *parent):
    QAbstractListModel{parent},
    iterableElem{ptr}
{   // empty, just like me inside
    // TODO: throw if it has no unofficial children
}

int ModelExIterable::rowCount(const QModelIndex &) const
{
    return iterableElem->unofficialChildrenCount();
}

QVariant ModelExIterable::data(const QModelIndex &index, int role) const
{
    if (index.isValid()) {
        if (role == Qt::DecorationRole) {
            auto sharedImage = iterableElem->getUnofficialChild(index.row());

            QImage img = std::move(static_cast<QImage>(*sharedImage));
            return QIcon(QPixmap::fromImage(img));
        } else if (role == Qt::SizeHintRole) {
            // TODO: implement it correctly
            return QSize{128,128};
        }
    }

    return QVariant{};
}

Qt::ItemFlags ModelExIterable::flags(const QModelIndex &) const
{
    return Qt::ItemIsSelectable;
}
