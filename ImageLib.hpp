#ifndef IMAGELIB_HPP_ZAL_2_INCLUDED
#define IMAGELIB_HPP_ZAL_2_INCLUDED

#include "AbstractThree.hpp"
#include <memory>
#include <array>

#include <QImage>

#include <iostream>

class Imageset;

/*******************************************************************************
 * Class Image
 * Application-specific class to read dataset arrays as images
 ******************************************************************************/

class Image: public HDF::Element
{
public:
    Image(Imageset &, hid_t hyperslab, const hsize_t dimensions[3]);
    // You're better than that. We can live without copying huge objects.
    Image           (const Image &) = delete;
    Image &operator=(const Image &) = delete;

    // TODO: implement two below
    // Image (Image &&);
    // Image &operator=(Image &&);
    virtual ~Image();

    virtual H5I_type_t getType() const
    {
        return H5I_DATASPACE;
    }

    // Convert image into Qt-type bitmap
    operator QImage() const;
private:
    // Information about underlying file size among with amount of colors
    hsize_t dataDimensions[3];
    // Raw data
    unsigned char *data;
};

// Imageset is a special iterable.
// It does not return iterable, it returns a dataspace that's element
// Please note that it does not represent universal dataset
// TODO: implement this as universal dataset
class Imageset: public HDF::Iterable
{
public:
    Imageset():
        HDF::Iterable{},
        dataspaceId{0}
    {
    }

    Imageset(hid_t rootId, const std::string &name):
        Iterable{H5Dopen(rootId, name.c_str(), H5P_DEFAULT)},
        dataspaceId{0}
    {
    }

    Imageset(const Imageset &)             = delete;
    Imageset &operator= (const Imageset &) = delete;

    virtual ~Imageset() noexcept
    {
        H5Dclose(getId());
    }

    virtual H5I_type_t getType() const
    {
        return H5I_DATASET;
    }
protected:
    // !!! this function is highly time-expensive as it reads megabytes of data
    //     into the memory
    //     it was not supposed to, but meh
    // TODO: optimize it
    virtual void iterate();
    hid_t dataspaceId;
};



#endif /* end of include guard: IMAGELIB_HPP_ZAL_2_INCLUDED */
