#include "AbstractThree.hpp"
#include "HDFClasses.hpp"
#include "ImageLib.hpp"

namespace HDF
{

// Regular + and - operators. Nothing to see here. Just carry on
IterableIterator operator+(const IterableIterator &it, std::size_t by) { return IterableIterator{it.within + by}; }
IterableIterator operator+(std::size_t by, const IterableIterator &it) { return IterableIterator{it.within + by}; }

IterableIterator operator-(const IterableIterator &it, std::size_t by) { return IterableIterator{it.within - by}; }
IterableIterator operator-(std::size_t by, const IterableIterator &it) { return IterableIterator{it.within - by}; }

// Factory function for HDF5 elements
void Iterable::iterate()
{
    if (isIterated())
        return;

    // Call a C function, H5Literate, with a lambda that creates all elements
    herr_t errval = H5Literate( getId(),            // what to iterate
                                H5_INDEX_NAME,      // order of iteration
                                H5_ITER_NATIVE,
                                NULL,
                                [] (hid_t rootId, const char *named, const H5L_info_t *, void *lambdaSub) -> herr_t
                                {
                                    try {
                                        // Create element
                                        auto vectorPtr = (std::vector<std::shared_ptr<Element>> *) lambdaSub;
                                        auto aytMade = aytMake<Element>(rootId, named);
                                        // Turn it into element and push it back
                                        std::shared_ptr<Element> shared{aytMade};
                                        vectorPtr->push_back(shared);

                                        return 0;
                                    } catch (...) { // we are not allowed to throw inside C library!
                                        return -1;
                                    }
                                },
                                (void *) &subelements // array to pass to lambda
                              );

    // but we are able to throw here, and we will
    if (errval < 0)
        // TODO: proper exceptions
        throw std::invalid_argument{"one of iteration elements could not be loaded"};
    else
        valid = true;
}

// TODO: make it better, using properly written AYT code
// To read a structure from file, we need to know its type. This function deduces that
// name "AYT" comes from a piece of code that I wrote and send to prof. Zieliński
// as a proof of concept for partially virtual constructor
template <typename T>
T *aytMake (hid_t rootId, const std::string &name)
{
    H5O_info_t buffer;
    H5Oget_info_by_name(rootId, name.c_str(), &buffer, H5P_DEFAULT);

    switch (buffer.type) {
    case H5O_TYPE_GROUP:
        return new Group{rootId, name};
    case H5O_TYPE_DATASET:
        return new Imageset{rootId, name};
    default:
        // TODO: proper exceptions
        throw std::invalid_argument{"element type could not be handled"};
    }
}

} // namespace HDF
