#include "ImageLib.hpp"
#include "HDFClasses.hpp"

#include <vector>

constexpr hsize_t THE_ONLY_ALLOWED_COUNT_OF_DIMS_IN_DATASET = 4;

using namespace HDF;

void Imageset::iterate()
{
    if (isIterated())
        return;

    // Get dataset's dimensions
    // Note that there should be only 4 dimensions and last one is [3]
    // TODO: strict error checking for aforementioned
    dataspaceId = H5Dget_space(getId());
    hsize_t dimSizes[THE_ONLY_ALLOWED_COUNT_OF_DIMS_IN_DATASET] = {0};

    H5Sget_simple_extent_dims(dataspaceId, dimSizes, nullptr);

    // Prepare for bulk insertion of objects
    // !!! iterator invalidation line ------------------------------------------
    subelements.reserve(dimSizes[0]);

    // Prepare data for hyperslab iteration
    //                  image #, X pixels,        Y pixels,       color data
          hsize_t start[4] = {1, 1,               1,              1};             // Start for slab iteration (1st will cahnge)
    const hsize_t count[4] = {1, dimSizes[1],     dimSizes[2],    dimSizes[3]},   // Amount of data to fetch
                  newCount[3] = {dimSizes[1],     dimSizes[2],    dimSizes[3]};   // amount of data to write

    for (/* start[0] */; start[0] < dimSizes[0]; ++start[0]) {
        // Part 1. Create hyperslab to read from
        H5Sselect_hyperslab(dataspaceId, H5S_SELECT_SET, start, nullptr, count, nullptr);

        // Part 2. Create hyperslab for image to play on.
        hid_t slab = H5Screate_simple(THE_ONLY_ALLOWED_COUNT_OF_DIMS_IN_DATASET - 1,
                                      newCount,
                                      nullptr
                                     );

        // Part 3. I guess there's no need to cut it. Create image with this slab
        Image *createdFromHyperslab = new Image{*this, slab, newCount};
        subelements.emplace_back(createdFromHyperslab);

        // dataspaceId will be clased by imageLib
        // slabs will be closed by Images taking care of them now
    }
    valid = true;
}

Image::Image(Imageset &refRoot, hid_t hyperslab, const hsize_t dimensions[3]):
    Element{hyperslab},
    dataDimensions{dimensions[0], dimensions[1], dimensions[2]},
    data(new unsigned char[dimensions[0] * dimensions[1] * dimensions[2]]) // !!!
{
    // TODO: error checking
    if (dimensions[2] != 3)
        throw "-2";     // TODO: c'mon fam, you're much better than throwing a goddamn integer

    // If everything's okay, read in the raw data
    H5Dread(refRoot.getId(), H5T_NATIVE_UCHAR, H5S_ALL, hyperslab, H5P_DEFAULT, data);
}

Image::operator QImage() const
{
    // TODO: BUG! This is not RGB32. So what the fuck is it?!
    // Ruled out all color models - are you sure it's not some intricate file format?
    return QImage{data, dataDimensions[0], dataDimensions[1], QImage::Format_RGB32};
}

Image::~Image()
{
    H5Sclose(getId());
    delete data;
}
