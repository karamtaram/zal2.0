#ifndef ZAL20_ABSTRACT_TREE_HPP_INCLUDED
#define ZAL20_ABSTRACT_TREE_HPP_INCLUDED

#include <hdf5.h>

#include <string>
#include <vector>
#include <stdexcept>
#include <memory>
#include <iostream>
#include <iterator>
#include <initializer_list>

namespace HDF
{

/*******************************************************************************
 * CLASS Element
 * Abstract class for any object that can be defined as HDF5 element
 * All of these functions are 2-3 lines long, so I didn't bother exporting them
 * to a .cpp file
 *******************************************************************************/
class Element
{
public:
    // Returns type of currently contained element
    // Obviously needs to be overloaded
    virtual H5I_type_t getType() const = 0;

    // Returns internal id of the element
    virtual hid_t getId() const
    {
        return elemId;
    }

    // Returns full path to element, with previous subdirectiories
    virtual std::string getPath() const
    {
        char buf[1024];
        H5Iget_name(elemId, buf, 1024);
        return std::string{std::move(buf)};
    }

    // Returns only last part of path (name of the element itself)
    virtual std::string getName() const
    {
        std::string toTrunc = std::move(getPath());
        auto lastSlashPos = toTrunc.find_last_of('/');
        return std::string{toTrunc, lastSlashPos + 1};
    }

    virtual bool isSame(const Element &rhs) const
    {
        return elemId == rhs.elemId;
    }

    virtual ~Element() noexcept = default;
protected:
    // The element should not be constructed by a class that is not a member
    // of HDF5 hierarchy
    // Either inherit or gtfo (but actually this is ensured by pure virtual function,
    // so it's just kind of syntactic sugar)
    Element(hid_t id = 0):
        elemId(id)
    {   // empty
    }
private:
    // Internal ID of the element from C version of HDF5 library
    hid_t elemId;
};

/*******************************************************************************
 * CLASS IterableIterator
 *
 * Iterator for containers (eg. Iterable) which contain Element type objects
 * It is not official random access iterator (lacks operator[]), but I still love
 * to use it
 *
 * TODO: make this official, full RandomAccessIterator
 *******************************************************************************
 */

class Iterable;

class IterableIterator: public std::iterator<std::random_access_iterator_tag,   // not fully, see frame above
                                             Element,
                                             std::ptrdiff_t,
                                             std::shared_ptr<Element>,
                                             Element &
                                            >
{
public:
    friend class Iterable;

    // Initialize empty iterator
    explicit IterableIterator():
        within{}
    {   // empty
    }

    // It's just wraparound, so typical copy/move is sufficient
    // TODO: move semantics
    IterableIterator (const IterableIterator &)            = default;
    IterableIterator &operator= (const IterableIterator &) = default;
    ~IterableIterator ()                                   = default;

    // Underlying iterator is in fact pointer to a pointer
    reference operator*() const { return **within; }
    pointer operator->() const  { return *within; }
    pointer getRaw() const      { return *within; }

    // Typical pre-incrementation operators - just move underlying and advance yourself...
    IterableIterator &operator++() { ++within; return *this; }
    IterableIterator &operator--() { --within; return *this; }

    // and typical post-incrementation
    IterableIterator operator++ (int) { IterableIterator tmp{*this}; ++within; return tmp; }
    IterableIterator operator-- (int) { IterableIterator tmp{*this}; --within; return tmp; }

    // Typical jump across, C-style - no bounds checking
    IterableIterator &operator+= (std::size_t by) { within += by; return *this; }
    IterableIterator &operator-= (std::size_t by) { within -= by; return *this; }

    // Calculate pointer difference
    difference_type operator- (const IterableIterator &rhs) const { return within - rhs.within; }

    // Comparison operators. Implement only == and < to access underlying objects, define rest as their logical conversion
    // WARNING: as with C pointers, result of such comparision is undefined within two different levels of file
    bool operator==(const IterableIterator &rhs) const { return within == rhs.within; }
    bool operator!=(const IterableIterator &rhs) const { return !(*this == rhs); }

    bool operator< (const IterableIterator &rhs) const { return within < rhs.within; }
    bool operator<=(const IterableIterator &rhs) const { return (*this < rhs) || (*this == rhs); }

    bool operator> (const IterableIterator &rhs) const { return !(*this <= rhs); }
    bool operator>=(const IterableIterator &rhs) const { return !(*this <  rhs); }

    // TODO: implement array operator (just to make this official RandomAccessIterator, not necessary for this application)

    // Also, addition and subtraction as external operations:
    friend IterableIterator operator+(const IterableIterator &it, std::size_t by);
    friend IterableIterator operator+(std::size_t by, const IterableIterator &it);

    friend IterableIterator operator-(const IterableIterator &it, std::size_t by);
    friend IterableIterator operator-(std::size_t by, const IterableIterator &it);
protected:
    // This class should be initialized by foreigners only by copying
    explicit IterableIterator(const std::vector<pointer>::iterator &in):
        within{in}
    {   // empty
    }
private:
    // Contains an array to all elements that are in the same "directory"
    std::vector<pointer>::iterator within;
};

/*******************************************************************************
 * CLASS Iterable
 *
 * Abstract class for any object that is an iterable HDF object. By "iterable"
 * I mean that it either has directly groups or datasets inside, or can be partitioned
 * into several datasets
 *
 * Please note that implementation of all container functions is up to deriving class
 * (File and Group however don't change it)
 *******************************************************************************
 */

class Iterable: public Element
{
public:
    friend class IterableIterator;

    typedef std::shared_ptr<Element> elementPtr;

    // Standard 6: default ctor (almost "= default"), the rest is default
    //  - we're more than happy with memberwise copy
    Iterable():
        valid{false},
        subelements{}
    {   // empty
    }
    virtual ~Iterable()                     = default;
    Iterable(const Iterable &)              = default;
    Iterable(Iterable &&)                   = default;
    Iterable &operator=(const Iterable &)   = default;
    Iterable &operator=(Iterable &&)        = default;

    // Most elementary container interface functions
    // Nothing interesting is going on here
    // It just calls respective functions on an underlying container, but ensuring
    // before hand that it is populated
    // TODO: full container interface: operator[], at(), front(), back(), insert(), erase(), clear(), push_back(), pop_back()
    // TODO: virtual constIterableIterator cbegin() const;
    // TODO: virtual constIterableIterator cend()   const;
    virtual std::size_t size() { iterate(); return subelements.size();  }
    virtual bool empty()       { iterate(); return subelements.empty(); }

    virtual IterableIterator  begin()  { iterate(); return IterableIterator{subelements.begin()}; }
    virtual IterableIterator  end()    { iterate(); return IterableIterator{subelements.end()}; }

    virtual elementPtr operator[] (std::size_t index)
    {
        iterate();
        return subelements[index];
    }

    // Unloads all elements from iterable cache
    // Note that it does not affect the object in the database
    virtual void purge()
    {
        valid = false;
        subelements.clear();
    }
protected:
    Iterable(hid_t id):
        Element{id},
        valid{false},
        subelements{}
    {
    }

    virtual bool isIterated() const
    {
        return valid;
    }

    // Forces iterable to perform first stage loading of elements
    // By first stage loading, I mean whatever objects feels like doing that isn't
    // excessively time expensive to provide size and iterators
    // Should throw if loading was impossible to perform
    virtual void iterate();

    // TODO: make this class const-correct
    // TODO: encapsulation?

    // Stores information whether iterate() has been called
    bool valid;

    // Underlying data structure
    std::vector<elementPtr> subelements;
};

} // end of namespace

#endif /* end of include guard: ZAL20_ABSTRACT_TREE_HPP_INCLUDED */
