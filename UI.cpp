#include "UI.hpp"

zalMain::zalMain():
    QMainWindow{nullptr},
    data{nullptr},
    currentImg{nullptr},
    ui{new Ui::MainWindow}
{
    ui->setupUi(this);
    setupActions();
}

void zalMain::setupActions()
{
    QObject::connect(ui->action_Open_file,  SIGNAL(triggered()), this, SLOT(openFile()));
    QObject::connect(ui->action_Close_file, SIGNAL(triggered()), this, SLOT(closeFile()));
    QObject::connect(ui->action_Quit,       SIGNAL(triggered()), qApp, SLOT(quit()));
    QObject::connect(ui->treeView,          &QTreeView::doubleClicked, this, &zalMain::loadImageset);
}

void zalMain::openFile()
{
    QString filename = QFileDialog::getOpenFileName(this);
    if (!filename.isEmpty()) {
        HDF::File *file = new HDF::File{filename.toStdString()};

        // Set up data models
        DataTree *newData = new DataTree{file, this};
        ui->treeView->setModel(newData);
        if (data)
            delete data;
        data = newData;

        // Enable appropriate buttons
        ui->action_Close_file->setEnabled(true);
    }
}

void zalMain::closeFile()
{
    // Drop all data models
    delete data;
    data = nullptr;

    // Disable appropriate buttons
    ui->action_Close_file ->setEnabled(false);
    ui->actionExport_to   ->setEnabled(false);
    ui->actionDeselect_all->setEnabled(false);
}

void zalMain::loadImageset(const QModelIndex &model)
{
    // Verify that data are correct
    if (!model.isValid())
        return;

    IterableWrapper *modelPtr = static_cast<IterableWrapper *>(model.internalPointer());
    if ((!modelPtr) || (!modelPtr->hasUnofficialChildren()))
        return;

    // K. Data are correct. Buckle up, making a list view!
    currentImg = new ModelExIterable{modelPtr, data};
    ui->imageSelector->setModel(currentImg);

    // can't stand waiting for C++17's fold expressions
    // TODO: C++17 is up. Do everything you can with fold expressions
    ui->actionExport_to   ->setEnabled(true);
    ui->actionDeselect_all->setEnabled(true);
}
