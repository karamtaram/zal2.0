#ifndef ZAL20_HDF_CLASSES_INCLUDED
#define ZAL20_HDF_CLASSES_INCLUDED

#include "AbstractThree.hpp"
//#include "zusammen/ayt.hpp"

// TODO: This class accidentally has simpleAYT implemented, export it!

#include <string>

namespace HDF
{

// File which is treated as a regular container
class File: public Iterable
{
public:
    File() = default;
    // TODO: const-correctness
    File(const std::string &filename, unsigned int hdfFileFlags = H5F_ACC_RDWR):
        Iterable{H5Fopen(filename.c_str(), hdfFileFlags, H5P_DEFAULT)}
    {
    }

    virtual ~File() noexcept
    {
        H5Fclose(getId());
    }

    virtual H5I_type_t getType() const
    {
        return H5I_FILE;
    }
};

// HDF5 group which is treated as a regular container
class Group: public Iterable
{
public:
    // TODO: create if does not exist
    Group() = default;
    Group(hid_t rootId, const std::string &name):
        Iterable{   // Create if doesn't exists, otherwise just open..
                    (H5Lexists(rootId, name.c_str(), H5P_DEFAULT) > 0)
                    ?   (H5Gopen  (rootId, name.c_str(), H5P_DEFAULT))
                    :   (H5Gcreate(rootId, name.c_str(), H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT))
                }
    {
    }

    virtual ~Group() noexcept
    {
        H5Gclose(getId());
    }

    virtual H5I_type_t getType() const
    {
        return H5I_GROUP;
    }
};

// F-n to create Group and File and store it as Element class
template <typename T>
T *aytMake (hid_t rootId, const std::string &name);
} // namespace hdf

#endif /* end of include guard: ZAL20_HDF_CLASSES_INCLUDED */
