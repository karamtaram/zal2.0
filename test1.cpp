#include "HDFClasses.hpp"
using namespace HDF;

#include <iostream>
using std::cout;
using std::cin;
using std::endl;

#include <string>
#include <algorithm>

void describeIteration(Element &sub)
{
    cout << sub.getName() << ", isFile: " << (sub.getType() == H5I_FILE) << endl;

    Element *subptr = &sub;
    cout << subptr << endl;

    auto nextLevel = dynamic_cast<Iterable *>(subptr);
    cout << nextLevel << endl;
    if (nextLevel) {
        cout << "ptr correct" << endl;
        std::for_each(nextLevel->begin(), nextLevel->end(), describeIteration);
    }
}

int main()
{
    cout << "Gimmeh filename: ";
    std::string fn;
    cin >> fn;

    cout << "Opening file" << endl;
    File hdfFile{fn};
    cout << "Scanning file for elements:" << endl;
    describeIteration(hdfFile);
}
