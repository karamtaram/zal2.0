#ifndef ITERABLE_WX_ZAL_20_DEFINED
#define ITERABLE_WX_ZAL_20_DEFINED

/* IterableWx - a class to wrap HDF5 model for GUI
 * It's named "IterableWx", since originally it was designed for WxWidgets
 */

#include <memory>
#include <algorithm>
#include <string>
#include <stdexcept>

#include <QAbstractItemModel>

#include "HDFClasses.hpp"
#include "ImageLib.hpp"


constexpr std::size_t DEFAULT_COLUMN_COUNT = 6;

// A model class that emulates shared pointer and tree model element for Qt library
// together with images contained in nodes
// TODO: make it more abstract
struct IterableWrapper
{
    explicit IterableWrapper(HDF::Iterable *root, IterableWrapper *parentItem = nullptr);
    ~IterableWrapper();

    IterableWrapper *child(int row);
    int childCount() const;
    int columnCount() const
    {
        return 1;
    }

    QVariant data(int column) const;
    int row() const;
    IterableWrapper *parentItem();
    HDF::Iterable *raw()
    {
        return internalPtr;
    }

    // xD https://youtu.be/31BpTxNOo6k?t=53s
    // "Unofficial child" is a child that belongs to Iterable,
    // but should not be listed in a tree, so in our application - images

    // TODO: sleep at night instead of writing retarded comments and watching Youtube
    bool hasUnofficialChildren() const;
    int unofficialChildrenCount() const;
    std::shared_ptr<Image> getUnofficialChild(int row) const;

protected:
    bool ensureIterated() const;
private:
    mutable HDF::Iterable *internalPtr;
    mutable IterableWrapper *parentPtr;
};

// A controller class that merges HDF5 model with Qt interface
class DataTree: public QAbstractItemModel
{
    Q_OBJECT
public:
    // TODO: replace these typedefs with something more handy
    typedef HDF::Iterable *pointer;
    typedef std::shared_ptr<HDF::Element> subelemPointer;

    // Read the model into the memory
    DataTree read(pointer rootLevel);

    explicit DataTree(pointer rootlevel = nullptr, QObject *parent = 0):
        QAbstractItemModel{parent},
        first{(rootlevel) ? new IterableWrapper{rootlevel} : nullptr}
    {
    }
    virtual ~DataTree()
    {
        delete first;
    }

    DataTree(const DataTree &)            = delete;
    DataTree &operator=(const DataTree &) = delete;


    // Qt-specific controller functions
    virtual Qt::ItemFlags flags(const QModelIndex &index) const;
    virtual QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;

    QModelIndex index (int row, int column, const QModelIndex &parent) const;
    QModelIndex parent(const QModelIndex &index) const;

    // There's no header for the tree
    virtual QVariant headerData(int, Qt::Orientation, int) const
    {
        return QVariant{};
    }

    virtual int rowCount(const QModelIndex &parent = QModelIndex()) const;
    virtual int columnCount(const QModelIndex &parent = QModelIndex()) const;

    bool ensureIterated() const;
private:
    // TODO: const-correctness
    // Actual sublevel inside the WXIterable
    IterableWrapper *first;
};

// Model ex iterable - a class turning iterable node into table model
// name inspired by "deus ex machina"
// WARNING: do not delete iterableElem in destructor!
// TODO: implement this as shared/weak ptr
class ModelExIterable: public QAbstractListModel
{
    Q_OBJECT
public:
    ModelExIterable(const IterableWrapper *ptr = nullptr, QObject *parent = 0);

    virtual int rowCount(const QModelIndex &parent = QModelIndex()) const;
    virtual QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;

    virtual Qt::ItemFlags flags(const QModelIndex &index) const;
    virtual QVariant headerData(int, Qt::Orientation, int) const
    {
        return QVariant{};
    }
private:
    const IterableWrapper *iterableElem;
};

#endif /* end of include guard: ITERABLE_WX_ZAL_20_DEFINED */
